import { Component, OnInit } from '@angular/core';
import { FORM_DIRECTIVES } from '@angular/forms';
import { ItemsService, IItem } from "../items.service";
import { RatingComponent } from 'ng2-bootstrap';

@Component({
    moduleId: module.id,
    selector: 'items-list',
    directives: [RatingComponent, FORM_DIRECTIVES],
    providers: [ItemsService],
    templateUrl: 'items.list.component.html'
})
export class ItemsListComponent implements OnInit {

  title: String = 'My Cool List Of Items';
  items: Array<IItem>;

  constructor(private itemsService: ItemsService) { }

  ngOnInit() {
    this.itemsService.getItems().subscribe((items) => this.items = items);
  }

}
