import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'my-app',
    template: `<div class="container">
                 <h1>Item List</h1>
                 <router-outlet></router-outlet>
               </div>`,
    directives: [RouterOutlet]
})

export class AppComponent { }
