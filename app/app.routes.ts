import { Routes, RouterModule } from '@angular/router';

import {ItemsListComponent} from "./items.list/items.list.component";
import {ItemDetailsComponent} from "./item.detail/item.detail.component";

const routes: Routes = [
  { path: 'itemslist', component: ItemsListComponent },
  { path: 'itemdetails/:id', component: ItemDetailsComponent },
  { path: '', redirectTo: '/itemslist', pathMatch: 'full' }
];

export const routing = RouterModule.forRoot(routes);
