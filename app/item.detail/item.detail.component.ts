import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {ItemsService, IItem} from "../items.service";

@Component({
    moduleId: module.id,
    selector: 'item-detail',
    providers: [ItemsService],
    templateUrl: 'item.detail.component.html'
})
export class ItemDetailsComponent implements OnInit {

  item: IItem;

  constructor(private route: ActivatedRoute, private itemsService: ItemsService) { }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.itemsService.getItemById(params['id']).subscribe((item) => {
        this.item = item;
      });
    })
}

}
