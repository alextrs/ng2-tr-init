import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/operator/map';

export interface IItem {
  id: String;
  title: String;
  description: String;
  image: String;
}

@Injectable()
export class ItemsService {

  constructor(private http: Http) {
  }

  getItems(): Observable<Array<IItem>> {
    return this.http.get('/data/items.json').map((res: Response) => res.json());
  }

  getItemById(itemId: String): Observable<IItem> {
    return this.getItems().map((items) => {
      let activeItem: IItem = void 0;
      items.forEach(function(item) {
        if (item.id === itemId) {
          activeItem = item;
        }
      });
      return activeItem;
    });
  }

}
